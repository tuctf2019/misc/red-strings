# Red Yarn

## Challenge description
We like the simple things in life -- this file went back to the basics!

## Required files for hosting
`DEBUG.COM`

## Hosting instructions
- The included file should be distributed with the challenge description/flag submission platform

## Hints
- Look at what's in the file first, see what you can find!

## Flag 
`TUCTF{D0NT_F0RG3T_TH3_B4S1CS!}`